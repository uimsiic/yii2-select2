<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2019-03-28
 * Time: 12:39
 */

namespace junati\select2;

use yii\web\AssetBundle;

/**
 * Asset bundle for [[JISelect2]] Widget.
 *
 * @author Vishal Parashar <vishal@junati.com>
 * @since 1.0
 */
class JISelect2Asset extends AssetBundle
{
    public $css = [
        'css/select2.css',
    ];

    public $js = [
        'js/select2.js',
        'js/jiSelect2widget.js',
    ];

    public $depends = [
        'yii\web\YiiAsset'
    ];

    public function __construct($config = [])
    {
        parent::__construct($config);
        $this->sourcePath = __DIR__ . '/assets';
    }

}

